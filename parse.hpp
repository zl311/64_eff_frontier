#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>

void splitcomma(string line, vector<string> &v);
vector<Asset> getDataFromUni(ifstream & ifs);
Eigen::MatrixXd getDataFromCorr(ifstream &ifs,size_t num);
int find_min_weight(Eigen::VectorXd w);
double optimalUnlimited(Portfolio &p, double ror);
double optimalLimited(Portfolio &p, double ror);
double gradDesUnlimited(Portfolio &p, double ror);
double gradDeslimited(Portfolio &p, double ror);
//void printAns(vector<Asset> & assets);
#endif
