#include "parse.hpp"
#include "asset.hpp"
#include "portfolio.hpp"
#include <vector>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <Eigen/Core>
//#include <Eigen/Dense>

using namespace std;
void splitcomma(string line, vector<string> &v){
  int pos = 0;
  int start = -1;
  //add a comma to ensure loop to the end of the line
  line.append(",");
  while((pos = line.find(',', pos))!=-1){
    string str = line.substr(start+1, pos-start-1);  
    v.push_back(str);
    start = pos;
    pos++;
  }

  
}


vector<Asset> getDataFromUni(ifstream & ifs){
  vector<Asset> assets;
  string line;
  while(getline(ifs,line)){
    vector<string> split_line;
    splitcomma(line, split_line);
    if(split_line.size()!=3){
      cerr<<"wrong format at universe.csv"<<endl;
      exit(EXIT_FAILURE);
    }
    double r = strtod(split_line[1].c_str(),NULL);
    double v = strtod(split_line[2].c_str(),NULL);
    if(r==0.0 || v<=0.0){
      cerr<<"portfolio value type wrong"<<endl;
      exit(EXIT_FAILURE);
    }
    assets.push_back(Asset(split_line[0], r, v));
    
  }
  if(assets.size()==0){
    cerr<<"assets number is 0"<<endl;
    exit(EXIT_FAILURE);
  }
  return assets;

}
Eigen::MatrixXd getDataFromCorr(ifstream &ifs, size_t num){
  Eigen::MatrixXd corrMat(num, num);
  string line;
  size_t rows = 0;
  while(getline(ifs, line)){
    if(rows>=num){
      cerr<<"number of corr is larger than the assets size"<<endl;
      exit(EXIT_FAILURE);
    }
    vector<string> d;
    splitcomma(line,d);
    if(d.size()!=num){
      cerr<<"number of corr is different with the assets size"<<endl;
      exit(EXIT_FAILURE);
    }
    for(size_t cols=0;cols<d.size();cols++){
      double corr = strtod(d[cols].c_str(),NULL);
      if(corr==0.0){
	cerr<<"non-numeric data in the corr file"<<endl;
	exit(EXIT_FAILURE);
      }
      if(corr<-1.0 || corr>1.0){
	cerr<<"corr is out of range(<0 or >1)"<<endl;
	exit(EXIT_FAILURE);
      }
      corrMat(rows,cols)=corr;
    }
    
    rows++;  
  }

  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<=i;j++){
      if(fabs(corrMat(i,j) - corrMat(j,i)) > 0.00001 || fabs(corrMat(i,j)) > 1.00001){
	cerr<<"wrong range of corr."<<endl;
        exit(EXIT_FAILURE);
      }
    }
    if(fabs(corrMat(i,i) - 1) > 0.0001){
      cerr<<"dialog wrong corr"<<endl;
      exit(EXIT_FAILURE);
    }
  }

  
  if(rows< num){
      cerr<<"number of corr is less than the assets size"<<endl;
      exit(EXIT_FAILURE);
  }
  return corrMat;
}


double optimalUnlimited(Portfolio &p, double ror){

  size_t num = p.num;
  //1. calculate the MatA which include mat R and 1T
  Eigen::MatrixXd One = Eigen::MatrixXd::Ones(1,num);
  Eigen::MatrixXd R(1,num);
  for(size_t i=0;i<num;i++){
    R(i) = p.assets[i].ror;
  }
  Eigen::MatrixXd MatA(2,num);
  MatA<<One, R;

  //2.define the Matb
  Eigen::MatrixXd b(2,1);
  b<<1,ror;

  //3.define rhs
  Eigen::MatrixXd Zero = Eigen::MatrixXd::Zero(num,1);
  Eigen::MatrixXd T(num+2,1);
  T<<Zero,b;

  //4.define lhs

  /*
I want to solve the matrix equation: H(n+2 * n+2) * W(n+2 * 1) = T(n+2*1) 
    H           W       T
| σ   A^T |   | w |   | 0 |
|         | * |   | = |   |
| A   0   |   | λ |   | b |

   */

  Eigen::MatrixXd Cov(num, num);
  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<num;j++){
      Cov(i,j) = p.assets[i].vol * p.corr(i,j) * p.assets[j].vol;

    }
  }

  Eigen::MatrixXd Zero2 = Eigen::MatrixXd::Zero(2,2);
  Eigen::MatrixXd MatAT = MatA.transpose();

  Eigen::MatrixXd H(num+2, num+2);
  H<< Cov ,MatAT,
      MatA, Zero2;
  
  //5.create and get the ans vector
  Eigen::VectorXd W(num+2,1);
  W = H.fullPivHouseholderQr().solve(T);

  //6.weight is first num data, λ is last 2
  p.weight = W.head(num);
  p.cal_ror();
  p.cal_vol();
  return p.p_vol;
}


int find_min_weight(Eigen::VectorXd w){
  size_t n = w.size();
  int min_w = 0;
  int flag=1;
  
  for(size_t i=0;i<n;i++){
    if(w(i)<-0.0001){
      flag=0;
      if(w(i)<w(min_w)){
	min_w=i;
      }
    }
  }

  if(flag==1){
    return -1;
  }
  else{
    return min_w;
  }
  
}

double optimalLimited(Portfolio &p, double ror){
  size_t num = p.num;
  //  size_t n=p.num;
  //1. calculate the MatA which include mat R and 1T
  Eigen::MatrixXd One = Eigen::MatrixXd::Ones(1,num);
  Eigen::MatrixXd R(1,num);
  for(size_t i=0;i<num;i++){
    R(i) = p.assets[i].ror;
  }
  Eigen::MatrixXd MatA(2,num);
  MatA<<One, R;

  //2.define the Matb
  Eigen::MatrixXd b(2,1);
  b<<1,ror;

  //3.define rhs
  Eigen::MatrixXd Zero = Eigen::MatrixXd::Zero(num,1);
  Eigen::MatrixXd T(num+2,1);
  T<<Zero,b;

  //4.define lhs
  Eigen::MatrixXd Cov(num, num);
  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<num;j++){
      Cov(i,j) = p.assets[i].vol * p.corr(i,j) * p.assets[j].vol;

    }
  }

  Eigen::MatrixXd Zero2 = Eigen::MatrixXd::Zero(2,2);
  Eigen::MatrixXd MatAT = MatA.transpose();

  Eigen::MatrixXd H(num+2, num+2);
  H<< Cov ,MatAT,
      MatA, Zero2;

  //5.create and get the ans vector
  Eigen::VectorXd W(num+2,1);
  W = H.fullPivHouseholderQr().solve(T);
  

  for(size_t i=1;i<=num;i++){

    int ne_ix = find_min_weight(W.head(num));

    if (ne_ix!=-1){
      MatA.conservativeResize(MatA.rows()+1,MatA.cols());
      MatA.row(MatA.rows()-1).setZero();
      MatA(MatA.rows()-1, ne_ix) = 1;
      //      cout<<"MATA"<<MatA<<endl;  
      MatAT = MatA.transpose();
      
      b.conservativeResize(b.rows()+1,b.cols());
      b.row(b.rows()-1).setZero();
      
      
      Eigen::MatrixXd HH(num+i+2, num+i+2);
      Eigen::MatrixXd ZeroX = Eigen::MatrixXd::Zero(2+i,2+i);
      HH<< Cov,MatAT,
	MatA,ZeroX;
    

      Eigen::MatrixXd TT(num+2+i,1);
      TT<<Zero,b;
      W.resize(num+2+i,1);
      W = HH.fullPivHouseholderQr().solve(TT);

    }
    
    else{
      break;
    }
  }
  
  //6.weight is first num data, λ is last 2
  p.weight = W.head(num);
  p.cal_ror();
  p.cal_vol();
  return p.p_vol;
  
}
  



double gradDesUnlimited(Portfolio &p, double ror){

  double yita1 = 0.5;
  double yita2 = 0.005;

  size_t num = p.num;
  /*
  Algo:
    Xt-1 = Xt - η1(σxt +ATλt)
    λt+1 = λt + η2(Axt − b)
   */
  
  //1. calculate the MatA which include mat R and 1T                                                                                                         
  Eigen::MatrixXd One = Eigen::MatrixXd::Ones(1,num);
  Eigen::MatrixXd R(1,num);
  for(size_t i=0;i<num;i++){
    R(i) = p.assets[i].ror;
  }
  Eigen::MatrixXd MatA(2,num);
  MatA<<One, R;
  Eigen::MatrixXd MatAT = MatA.transpose();
  
  //2.define the Matb                                                                                                                                       
  Eigen::MatrixXd b(2,1);
  b<<1,ror;

  //3.define cov                                                                                                                                             
  Eigen::MatrixXd Cov(num, num);
  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<num;j++){
      Cov(i,j) = p.assets[i].vol * p.corr(i,j) * p.assets[j].vol;
    }
  }


  //4. initialize the w and λ
  Eigen::MatrixXd w = Eigen::MatrixXd::Zero(num,1);
  w(0,0) = 1;
  Eigen::MatrixXd lambda = Eigen::MatrixXd::Zero(2,1);

  size_t iter = 100000;
  
  for(size_t i=0;i<iter;i++){
    Eigen::MatrixXd wt = w;
    w = w - yita1*(Cov * w + MatAT * lambda);
    lambda = lambda + yita2*(MatA * wt - b);
    /*
    if(i % 1000 == 0){
      cout<<w<<endl;
      cout<<"---------------"<<(double)i/iter*100<<"%----------"<<endl;
    }

    */
    Eigen::MatrixXd delta_w = w-wt;
    int converge = 0;
    for(size_t i=0;i<num;i++){   
      if(abs(delta_w(i)) <= 1e-9){
	converge++;
      }
    }
    if(converge==6){break;}
  }
  p.weight = w;
  p.cal_ror();
  p.cal_vol();
  return p.p_vol;
  
}



double gradDeslimited(Portfolio &p, double ror){

  double yita1 = 0.5;
  double yita2 = 0.005;

  size_t num = p.num;
  /*
  Algo:
    Xt-1 = Xt - η1(σxt +ATλt)+
    λt+1 = λt + η2(Axt − b)
   */
  
//1. calculate the MatA which include mat R and 1T
  Eigen::MatrixXd One = Eigen::MatrixXd::Ones(1,num);
  Eigen::MatrixXd R(1,num);
  for(size_t i=0;i<num;i++){
    R(i) = p.assets[i].ror;
  }
  Eigen::MatrixXd MatA(2,num);
  MatA<<One, R;
  Eigen::MatrixXd MatAT = MatA.transpose();
  //2.define the Matb  
  Eigen::MatrixXd b(2,1);
  b<<1,ror;
  //3.define cov 
  Eigen::MatrixXd Cov(num, num);
  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<num;j++){
      Cov(i,j) = p.assets[i].vol * p.corr(i,j) * p.assets[j].vol;
    }
  }


  //4. initialize the w and λ
  Eigen::MatrixXd w = Eigen::MatrixXd::Zero(num,1);
  //  w(0,0) = 1;
  Eigen::MatrixXd lambda = Eigen::MatrixXd::Zero(2,1);

  size_t iter = 5000000;
  
  for(size_t i=0;i<iter;i++){
    Eigen::MatrixXd wt = w;
    w = w - yita1*(Cov * w + MatAT * lambda);
    lambda = lambda + yita2*(MatA * wt - b);

    for(size_t j=0;j<num;j++){
      if(w(j) <= 0){
        w(j)=0;
      }
    }
    /*
    if(i % 50000 == 0){
      cout<<w<<endl;
      cout<<"---------------iteration completed "<<(double)i/iter*100<<"%----------"<<endl;
    }
    */
    
    Eigen::MatrixXd delta_w = w-wt;
    int converge = 0;
    for(size_t i=0;i<num;i++){   
      if(abs(delta_w(i)) <= 1e-9){
	converge++;
      }
    }
    if(converge==6){break;}
  }
  p.weight = w;
  p.cal_ror();
  p.cal_vol();
  return p.p_vol;
  
}




//double optimalLimited(Portfolio &p, double ror){
