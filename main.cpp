#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iomanip>
#include <unistd.h>
#include <Eigen/Core>
#include "parse.hpp"
#include "portfolio.hpp"
#include "asset.hpp"
using namespace std;

int main(int argc,char **argv){
  //data prepartion process
  //1. check the number of arguments
  if(argc!=3 && argc!=4){
    cerr<<"wrong number of input"<<endl;
    return EXIT_FAILURE;
  }
  char o;
  bool isopt=false;
  //2. check the options format
  while((o = getopt(argc,argv,"r"))!=-1){
    switch(o){
    case 'r':
      isopt=true;
      break;
    case '?':
      cerr<<"wrong input of option"<<endl;
      return EXIT_FAILURE;
    }
  }

  //3. check the validation of files
  ifstream funi;
  ifstream fcorr;
  if(isopt==true){
    funi.open(argv[2],ios::in);
    fcorr.open(argv[3],ios::in);
  }
  else {
    funi.open(argv[1],ios::in);
    fcorr.open(argv[2],ios::in);
    
  }  
  if(!funi){
    cerr<<"fail to open the universe file."<<endl;
    return EXIT_FAILURE;  
  }  
  if(!fcorr){
    cerr<<"fail to open the correlation file."<<endl;
    return EXIT_FAILURE;  
  }

  //4. read data and establish portfolio
  vector<Asset> assets = getDataFromUni(funi);
  int assetNum = assets.size();
  Eigen::MatrixXd corr = getDataFromCorr(fcorr, assetNum);
  Portfolio p(assets, corr);
  cout<<"ROR,volatility"<<endl;
  cout.setf(ios::fixed);

  //  cout<<gradDeslimited(p,0.01)<<endl;

   
  for(double r=0.01;r<=0.263;r+=0.01){
    cout<< setprecision(1)<< r*100<<"%,";
    if(isopt==false){
      //      cout<<"Lagrange multipliers"<<endl;
      cout<< setprecision(2)<< optimalUnlimited(p,r)*100<<"%"<<endl;
      //      cout<<"gradient descent"<<endl;
      //cout<< setprecision(2)<< gradDesUnlimited(p,r)*100<<"%"<<endl;
    }

    
    else if(isopt==true){
      //cout<< setprecision(2)<< gradDeslimited(p,r)*100<<"%"<<endl;
      cout<< setprecision(2)<< optimalLimited(p,r)*100<<"%"<<endl;
      continue;
    }
  }
  
  return EXIT_SUCCESS;
}

