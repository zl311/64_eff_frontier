#include "portfolio.hpp"
#include <Eigen/Core>
Portfolio::Portfolio(vector<Asset> a,Eigen::MatrixXd c):assets(a),
						  corr(c){
  //cal_ror();
  //cal_vol();
  num = assets.size();
  //  weight = Eigen::MatrixXd::Zero(1,num);
						  }

void Portfolio::cal_ror(){
  double r = 0;
  for(size_t i=0;i<num;i++){
    r+= weight(i)*assets[i].ror;
  }
  p_ror = r;
}

void Portfolio::cal_vol(){
  double v = 0;
  for(size_t i=0;i<num;i++){
    for(size_t j=0;j<num;j++){
      v+=weight(i)*weight(j)*assets[i].vol*assets[j].vol*corr(i,j);     
    }
    p_vol = sqrt(v);
  }
}
