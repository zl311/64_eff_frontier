#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Dense>
#include "asset.hpp"
using namespace std;

class Portfolio{
public:
  vector<Asset> assets;
  Eigen::VectorXd weight;
  Eigen::MatrixXd corr;
  double p_ror;
  double p_vol;
  size_t num;

  
public:
  Portfolio(vector<Asset> a, Eigen::MatrixXd c);
  void cal_ror();
  void cal_vol();
};

#endif
